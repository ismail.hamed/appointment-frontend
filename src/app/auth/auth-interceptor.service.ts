import {Injectable} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpHeaders, HttpParams
} from '@angular/common/http';
import {take, exhaustMap} from 'rxjs/operators';

import {AuthService} from './auth.service';
import {environment} from '../../environments/environment';

const baseUrl = environment.baseUrl;

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.authService.user.pipe(
      take(1),
      exhaustMap(user => {
        const modifiedURLReq = req.clone(
          {url: baseUrl + req.url}
        );
        // Todo redirect to auth
        if (!user) {
          return next.handle(modifiedURLReq);
        }
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + user.access_token,
            'Content-Type': 'application/json',
          },
        );
        let modifiedReq = modifiedURLReq.clone({headers});
        const timezone = localStorage.getItem('timeZone');
        if (timezone) {
          const params = modifiedReq.params.append('timeZone', timezone);
          modifiedReq = modifiedReq.clone({params});
        }
        return next.handle(modifiedReq);
      })
    );
  }
}

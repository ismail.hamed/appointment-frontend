import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {User} from './user.model';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient,
              private router: Router) {
  }

  loginAsGuest(name: string, uuid: string) {
    const body = {
      firstName: name,
      uuid: uuid
    };
    const url = environment.loginAsGuest;
    return this.http.post<User>(url, body)
      .pipe(
        tap(resData => {

            this.router.navigate(['/']);
            this.handleAuthentication(resData);
          }
        )
      );
  }

  private handleAuthentication(user: User) {

    // const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
    // const user = new User(userId, tokenType, token, refreshToken, expirationDate);
    this.user.next(user);
    // this.autoLogout(expiresIn * 1000);
    localStorage.setItem('userData', JSON.stringify(user));
  }

  autoLogin() {

    const userData = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      {
        this.user.next(null);
        return;
      }
    }
    if (userData.access_token) {
      this.user.next(userData);
    }
  }

  logout() {
    return this.http.get('api/users/logout').pipe(
      tap(resData => {
          this.user.next(null);
          localStorage.removeItem('userData');
          this.router.navigate(['/auth']);
        }
      )
    );
  }
}

import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {FormControl, NgForm, Validators} from '@angular/forms';
import {UUID} from 'angular2-uuid';

@Component({
  selector: 'app-login-as-guest',
  templateUrl: './login-as-guest.component.html',
  styleUrls: ['./login-as-guest.component.css']
})
export class LoginAsGuestComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    const name = form.value.name;
    const uuid = UUID.UUID();
    this.authService.loginAsGuest(name, uuid).subscribe(response => {

      this.router.navigate(['/']);
    });
  }
}

export interface Me {
  identifier: number;
  firstName: string;
  lastName: string;
  email: string;
  status: number;
  imageLink?: any;
  emailVerified: boolean;
  creationDate: string;
  lastChange: string;
  deleteDate?: any;
}

export interface User {
  access_token: string;
  token_type: string;
  expires_in: number;
  me: Me;
}


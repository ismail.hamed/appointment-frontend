import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginAsGuestComponent} from './login-as-guest/login-as-guest.component';

const routes: Routes = [
    {
        path: 'auth',
        component: LoginAsGuestComponent
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {

}

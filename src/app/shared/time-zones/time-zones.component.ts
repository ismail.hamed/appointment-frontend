import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-time-zones',
  templateUrl: './time-zones.component.html',
  styleUrls: ['./time-zones.component.css']
})
export class TimeZonesComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

  onChange(value): void {
    localStorage.setItem('timeZone', value);
    alert('please refresh page after change time zone');
  }
}

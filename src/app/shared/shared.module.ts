import {NgModule} from '@angular/core';
import {TimeZonesComponent} from './time-zones/time-zones.component';
import {SpinnerComponent} from './spinner/spinner.component';

@NgModule({
  declarations: [
    TimeZonesComponent,
    SpinnerComponent,

  ],
  imports: [],
  exports: [
    TimeZonesComponent,
    SpinnerComponent,

  ]
})
export class SharedModule {
}

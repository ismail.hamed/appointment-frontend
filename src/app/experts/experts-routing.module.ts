import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExpertsComponent} from './experts.component';
import {AuthGurd} from '../auth/auth.gurd';
import {ExpertsResolverService} from './experts-resolver.service';
import {ExpertComponent} from './expert/expert.component';
import {TimeAvailableResolverService} from './expert/timeAvailable-resolver.service';

const routes: Routes = [
  {
    path: 'experts',
    component: ExpertsComponent,
    canActivate: [AuthGurd],
    resolve: {experts: ExpertsResolverService},
  },
  {
    path: 'experts/:id',
    component: ExpertComponent,
    canActivate: [AuthGurd],
    resolve: {timesAvailable: TimeAvailableResolverService},
  }


];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ExpertsRoutingModule {

}

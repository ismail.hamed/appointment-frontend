import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Expert, Experts} from './expert.model';

const EXPERTSURL = environment.experts;

@Injectable({
  providedIn: 'root'
})
export class ExpertsService {

  constructor(private http: HttpClient) {
  }

  fetchExperts(pageNumber: string = '1', perPage: string = '15') {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('include', 'workHours');
    httpParams = httpParams.set('page', pageNumber);
    httpParams = httpParams.set('per_page', perPage);
    const opts = {params: httpParams};
    return this.http.get<Experts>(EXPERTSURL, opts).pipe(map(res => {
      return res;
    }));
  }
}

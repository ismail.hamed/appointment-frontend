import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ExpertService} from './expert.service';
import {TimeAvailable, TimesAvailable} from './timeAvailable.model';

@Injectable({
    providedIn: 'root'
})
@Injectable()
export class TimeAvailableResolverService implements Resolve<TimesAvailable> {

    constructor(private expertService: ExpertService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<TimesAvailable> |
        Promise<TimesAvailable> |
      TimesAvailable {
        let id = route.params['id'];
        id = (!!id ? id : route.parent.params['id']);
        return this.expertService.getTimeAvailable(id);
    }
}

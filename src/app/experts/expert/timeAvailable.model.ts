export interface TimeSlot {
  from: string;
  to: string;
}

export interface TimeAvailable {
  duration: number;
  timeSlots: TimeSlot[];
}

export interface TimesAvailable {
  data: TimeAvailable[];
}

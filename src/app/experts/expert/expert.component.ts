import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {ExpertService} from './expert.service';
import * as moment from 'moment';

@Component({
  selector: 'app-expert',
  templateUrl: './expert.component.html',
  styleUrls: ['./expert.component.css']
})
export class ExpertComponent implements OnInit {
  selectedDate = new Date();
  timesAvailable;
  timesSlot;
  minDate = new Date();
  selectedDuration: string;
  selectedStartAt: string;
  message: string;

  constructor(private router: Router, private expertService: ExpertService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
        this.timesAvailable = data.timesAvailable.data;
        if (this.timesAvailable.length > 0) {
          this.timesSlot = this.timesAvailable[0].timeSlots;
          // this.selectedDuration = '0';
          // this.selectedStartAt = this.timesAvailable[0].timeSlots[0].from;
        } else {
          this.selectedDuration = null;
          this.selectedStartAt = null;
          this.timesSlot = [];

        }
      }
    );
  }

  onChange(value): void {
    this.timesSlot = this.timesAvailable[value].timeSlots;
    this.selectedStartAt = null;
    this.message = '';
  }

  onChangeTimeSlot($event): void {
    const from = $event.target.options[$event.target.options.selectedIndex].text;
    const date = moment(this.selectedDate).format('LL');
    this.message = date + ' from ' + from;
  }

  onSelectedDate(date): void {
    this.selectedDate = date;
    const expertId = this.route.snapshot.paramMap.get('id');
    this.expertService.getTimeAvailable(expertId, date).subscribe(res => {
      this.timesAvailable = res.data;
      if (this.timesAvailable.length > 0) {
        this.timesSlot = this.timesAvailable[0].timeSlots;
        // this.selectedDuration = '0';
        // this.selectedStartAt = this.timesAvailable[0].timeSlots[0].from;
      } else {
        this.selectedDuration = null;
        this.selectedStartAt = null;
        this.timesSlot = [];
      }
    });
  }

  booking(): void {
    this.message = '';
    if ((!this.selectedStartAt) || (!this.selectedDuration)) {
      alert('please select duration and time slot');
      return;
    }
    const date = this.selectedDate;
    const startAt = this.selectedStartAt;
    const duration = this.timesAvailable[this.selectedDuration].duration;
    const expertId = this.route.snapshot.paramMap.get('id');
    this.expertService.booking(expertId, date, duration, startAt).subscribe(res => {
      const result: any = res;
      if (result.status_code == 200) {
        alert('Your booking confirm');
      } else {
        alert(result?.message);
      }
      this.router.navigate(['/']);
    });
  }
}

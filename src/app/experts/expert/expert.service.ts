import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {TimesAvailable} from './timeAvailable.model';
import * as moment from 'moment';

const TIMEAVAILABLEURL = environment.timeAvailable;
const BOOKINGURL = environment.booking;

@Injectable({
  providedIn: 'root'
})
export class ExpertService {

  constructor(private http: HttpClient) {
  }

  getTimeAvailable(expertId: string, date: Date = new Date()) {
    const dateformat = moment(date).format('YYYY-MM-DD');
    const body = {
      date: dateformat,
    };
    const url = TIMEAVAILABLEURL.replace('{expert}', expertId);
    return this.http.post<TimesAvailable>(url, body).pipe(map(res => {
      return res;
    }));
  }

  booking(expertId: string, date: Date, duration: string, startAt: string) {
    const dateformat = moment(date).format('YYYY-MM-DD');
    startAt = moment(startAt, ['h:mm A']).format('HH:mm:ss');
    startAt = moment(dateformat).add(startAt, 'hours').format('YYYY-MM-DD HH:mm:ss');
    const body = {
      startAt: startAt,
      duration: duration,
    };
    const url = BOOKINGURL.replace('{expert}', expertId);
    return this.http.post<TimesAvailable>(url, body).pipe(map(res => {
      return res;
    }));
  }
}

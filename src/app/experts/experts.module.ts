import {NgModule} from '@angular/core';
import {ExpertsComponent} from './experts.component';
import {ExpertsRoutingModule} from './experts-routing.module';
import {CommonModule} from '@angular/common';
import { ExpertComponent } from './expert/expert.component';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    ExpertsComponent,
    ExpertComponent,
  ],
  imports: [
    ExpertsRoutingModule,
    CommonModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    FormsModule,
    SharedModule,
  ],
})
export class ExpertsModule {

}

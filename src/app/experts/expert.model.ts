export interface WorkHour {
  identifier: number;
  day: number;
  openAt: string;
  closeAt: string;
  creationDate: string;
  lastChange: string;
  deleteDate?: any;
}

export interface WorkHours {
  data: WorkHour[];
}
export interface Expert {
  identifier: number;
  firstName: string;
  lastName: string;
  email: string;
  status: number;
  imageLink: string;
  emailVerified: boolean;
  job: string;
  country: string;
  timeZone: string;
  creationDate: string;
  lastChange: string;
  deleteDate?: any;
  workHours: WorkHours;
}

export interface Links {
  previous: string;
  next: string;
}

export interface Pagination {
  total: number;
  count: number;
  per_page: number;
  current_page: number;
  total_pages: number;
  links: Links;
}

export interface Meta {
  pagination: Pagination;
}

export interface Experts {
  data: Expert[];
  meta: Meta;
}



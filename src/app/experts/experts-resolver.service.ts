import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Experts} from './expert.model';
import {ExpertsService} from './experts.service';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class ExpertsResolverService implements Resolve<Experts> {

  constructor(private expertsService: ExpertsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Experts> |
    Promise<Experts> |
    Experts {
    return this.expertsService.fetchExperts(route.queryParams.page, route.queryParams.per_page);
  }
}

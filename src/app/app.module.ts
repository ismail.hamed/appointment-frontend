import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {CoreModule} from './core.module';
import {AppRoutingModule} from './app-routing.module';
import {AuthModule} from './auth/auth.module';
import {ExpertsModule} from './experts/experts.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';
import * as fromApp from './store/app.reducer';
import {SharedModule} from './shared/shared.module';
import {StoreModule} from '@ngrx/store';
import {GlobalErrorHandler} from './global-error-handler';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    MatDatepickerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatNativeDateModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    CoreModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ExpertsModule,
    AuthModule,
    SharedModule,
    StoreModule.forRoot(fromApp.appReducer),

  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    }

  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ExpertsComponent} from './experts/experts.component';

const appRoutes: Routes = [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: '/experts',
    },
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
